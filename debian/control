Source: ros-urdf
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Jochen Sprickerhof <jspricke@debian.org>,
           Leopold Palomo-Avellaneda <leo@alaxarxa.net>
Build-Depends: debhelper-compat (= 13), catkin (>= 0.8.10-1~), liburdfdom-headers-dev, libboost-thread-dev, liburdfdom-dev, pluginlib-dev, librosconsole-bridge-dev, libroscpp-dev, ros-cmake-modules, libtinyxml2-dev, librostest-dev, libgtest-dev, python3-rostest <!nocheck>
Standards-Version: 4.6.0
Section: libs
Rules-Requires-Root: no
Homepage: http://wiki.ros.org/urdf
Vcs-Browser: https://salsa.debian.org/science-team/ros-urdf
Vcs-Git: https://salsa.debian.org/science-team/ros-urdf.git

Package: liburdf-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: liburdf1d (= ${binary:Version}), ${misc:Depends}, librosconsole-bridge-dev, libroscpp-dev, ros-cmake-modules, liburdfdom-headers-dev, liburdfdom-dev, libtinyxml2-dev, pluginlib-dev,
Description: Development files for ROS urdf library
 The Unified Robot Description Format (URDF) for the Robot Operating System
 (ROS) is an XML format for representing a robot model.
 This library provides a C++ parser for the URDF.
 .
 This package contains the development files for the library.

Package: liburdf1d
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Multi-Arch: same
Description: ROS urdf library
 The Unified Robot Description Format (URDF) for the Robot Operating System
 (ROS) is an XML format for representing a robot model.
 This library provides a C++ parser for the URDF.
 .
 This package contains the library.

Package: liburdf-parser-plugin-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends}, liburdf-dev
Description: Development files for ROS urdf_parser_plugin library
 The Unified Robot Description Format (URDF) for the Robot Operating System
 (ROS) is an XML format for representing a robot model.
 .
 This package contains a C++ base class for URDF parsers.
